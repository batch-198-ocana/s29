db.courses.insertMany(
    [
        {
            name:"HTML Basics",
            price:20000,
            isActive:true,
            instructor:"Sir Alvin"
        },
        {
            name:"CSS 101 + Flexbox",
            price:21000,
            isActive:true,
            instructor:"Sir Alvin"
        },
        {
            name:"JavaScript 101",
            price:32000,
            isActive:true,
            instructor:"Ma'am Tine"
        },
        {
            name:"Git 101, IDE and CLI",
            price:19000,
            isActive:false,
            instructor:"Ma'am Tine"
        },
        {
            name:"React.JS 101",
            price:25000,
            isActive:true,
            instructor:"Ma'am Miah"
        },
    ]
)

db.courses.find({$and:[{instructor:"Sir Alvin"},{price:{$gte:20000}}]},{_id:0,name:1,price:1})

db.courses.find({$and:[{instructor:"Ma'am Tine"},{isActive:false}]},{_id:0,name:1,price:1})

db.courses.find({$and:[{name:{$regex: 'r'}},{price:{$lte:25000}}]})

db.courses.updateMany({price:{$lt:21000}},{$set:{isActive:false}})

db.courses.deleteMany({price:{$gte:25000}})